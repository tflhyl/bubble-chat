//
//  NSDateFormatter+CRSExtension.h
//  bubble-chat
//
//  Created by Theodore Felix Leo on 24/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (CRSExtension)

+ (NSDateFormatter *)crs_sharedInstance;
- (NSDate *)crs_dateFromAPIString:(NSString *)string;
- (NSString *)crs_stringForTimestampFromDate:(NSDate *)date;

@end

//
//  UIImage+BBCBubble.h
//  bubble-chat
//
//  Created by Theodore Felix Leo on 24/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (BBCBubble)

+ (UIImage *)incomingBubbleImage;
+ (UIImage *)outgoingBubbleImage;

@end

//
//  main.m
//  bubble-chat
//
//  Created by Theodore Felix Leo on 23/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "AppDelegate.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }

}
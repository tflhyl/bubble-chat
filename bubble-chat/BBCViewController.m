//
//  BBCViewController.m
//  bubble-chat
//
//  Created by Theodore Felix Leo on 23/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//


#import "BBCViewController.h"

#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIKit+AFNetworking.h>
#import <PureLayout/PureLayout.h>
#import "BBCProductBar.h"
#import "BBCInputBar.h"
#import "NSDateFormatter+CRSExtension.h"
#import "CRSKeyboardController.h"

#define DATA_URL @"https://afternoon-reef-6580.herokuapp.com/chat"

@interface BBCViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, BBCInputBarDelegate, CRSKeyboardControllerDelegate>

@property (weak, nonatomic) BBCProductBar *productBar;
@property (weak, nonatomic) UICollectionView *collectionView;
@property (weak, nonatomic) BBCInputBar *inputBar;

@property (strong, nonatomic) NSMutableArray *chats;
@property (strong, nonatomic) NSDictionary *offer;

@property (strong, nonatomic) CRSKeyboardController *keyboardController;

@end

@implementation BBCViewController

- (void)dealloc {
    self.collectionView.dataSource = nil;
    self.collectionView.delegate = nil;
    [self.keyboardController endListeningKeyboard];
}

- (void)setOffer:(NSDictionary *)offer {
    _offer = offer;
    NSURL *productImageURL = offer[@"product_image_url"];
    NSString *productName = offer[@"product_name"];
    NSString *productPrice = offer[@"product_price"];
    [self.productBar setImageURL:productImageURL name:productName price:productPrice];
    
    self.title = offer[@"buyer_name"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    BBCProductBar *productBar = [[BBCProductBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 80)];
    [self.view addSubview:productBar];
    [productBar autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [productBar autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [productBar autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [productBar autoSetDimension:ALDimensionHeight toSize:80];
    self.productBar = productBar;
    
    BBCInputBar *inputBar = [[BBCInputBar alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds) - 50, CGRectGetWidth(self.view.bounds), 50)];
    inputBar.delegate = self;
    [self.view addSubview:inputBar];
    [inputBar autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [inputBar autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [inputBar autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [inputBar autoSetDimension:ALDimensionHeight toSize:50];
    self.inputBar = inputBar;
    
    UICollectionViewFlowLayout *collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:collectionViewLayout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.bounces = YES;
    collectionView.alwaysBounceVertical = YES;
    collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    collectionView.backgroundColor = [UIColor colorWithWhite:0.99 alpha:1];
    [self.view addSubview:collectionView];
    [collectionView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [collectionView autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [collectionView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:productBar];
    [collectionView autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:inputBar];
    self.collectionView = collectionView;
    
    self.keyboardController = [[CRSKeyboardController alloc] initWithTextView:inputBar.textView contextView:self.view delegate:self];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.keyboardController beginListeningKeyboard];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.keyboardController endListeningKeyboard];
}

- (void)loadData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:DATA_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        NSLog(@"JSON: %@", responseDict);
        NSDictionary *offer = responseDict[@"offer"];
        self.offer = @{
            @"buyer_name": offer[@"buyer_name"],
            @"buyer_image_url": [NSURL URLWithString:offer[@"buyer_image_url"]],
            @"product_name": offer[@"product_name"],
            @"product_image_url": [NSURL URLWithString:offer[@"product_image_url"]],
            @"product_price": offer[@"product_price"],
        };
        
        self.chats = [NSMutableArray array];
        NSDateFormatter *dateFormatter = [NSDateFormatter crs_sharedInstance];
        for (NSDictionary *chat in responseDict[@"chats"]) {
            NSDictionary *parsedChat = @{
                @"date": [dateFormatter crs_dateFromAPIString:chat[@"timestamp"]],
                @"message": chat[@"message"],
                @"type": chat[@"type"],
            };
            [self.chats addObject:parsedChat];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.chats.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO
    return nil;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO
    return CGSizeZero;
}

#pragma mark - BBCInputBarDelegate

- (void)inputBar:(BBCInputBar *)inputBar didSendMessage:(NSString *)message {
    NSLog(@"New Message: %@", message);
    // TODO
}

#pragma mark - CRSKeyboardControllerDelegate

- (void)keyboardDidChangeFrame:(CGRect)keyboardFrame {
    NSLog(@"Keyboard frame %@", NSStringFromCGRect(keyboardFrame));
    // TODO
}

@end
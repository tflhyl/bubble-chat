//
//  BBCProductBar.h
//  bubble-chat
//
//  Created by Theodore Felix Leo on 23/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BBCProductBar : UIView

@property (weak, nonatomic, readonly) UIImageView *imageView;
@property (weak, nonatomic, readonly) UILabel *nameLabel;
@property (weak, nonatomic, readonly) UILabel *priceLabel;

- (void)setImageURL:(NSURL *)imageURL name:(NSString *)name price:(NSString *)price;

@end

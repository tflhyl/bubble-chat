//
//  CRSKeyboardController.m
//  Snapsell
//
//  Created by Theodore Felix Leo on 6/6/14.
//
//

#import "CRSKeyboardController.h"

#define KVO_FRAME NSStringFromSelector(@selector(frame))
#define KVO_CENTER NSStringFromSelector(@selector(center))

#define IOS_8_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)

@interface CRSKeyboardController ()

@property (weak, nonatomic) UITextView *textView;
@property (weak, nonatomic) UIView *contextView;
@property (weak, nonatomic) id<CRSKeyboardControllerDelegate> delegate;

@property (weak, nonatomic) UIView *keyboardView;

@end

@implementation CRSKeyboardController

- (instancetype)initWithTextView:(UITextView *)textView contextView:(UIView *)contextView delegate:(id<CRSKeyboardControllerDelegate>)delegate {
    NSParameterAssert(textView != nil);
    NSParameterAssert(contextView != nil);
    
    self = [super init];
    if (self) {
        _textView = textView;
        _contextView = contextView;
        _delegate = delegate;
    }
    return self;
}

- (instancetype)initWithContextView:(UIView *)contextView delegate:(id<CRSKeyboardControllerDelegate>)delegate {
    NSParameterAssert(contextView != nil);
    
    self = [super init];
    if (self) {
        _contextView = contextView;
        _delegate = delegate;
    }
    return self;
}

- (void)dealloc {
    [self unregisterKeyboardNotifications];
    if (self.textView) {
        self.textView.inputAccessoryView = nil;
        self.keyboardView = nil;
    }
    
    _textView = nil;
    _contextView = nil;
    _delegate = nil;
}

- (void)beginListeningKeyboard {
    if (self.textView) self.textView.inputAccessoryView = [[UIView alloc] init];
    
    [self registerKeyboardNotifications];
}

- (void)endListeningKeyboard {
    [self unregisterKeyboardNotifications];
    if (self.textView) {
        self.textView.inputAccessoryView = nil;
        self.keyboardView = nil;
    }
}

#pragma mark - Notifications

- (void)registerKeyboardNotifications {
    [self unregisterKeyboardNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unregisterKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    [self handleKeyboardNotification:notification animate:YES completion:nil];
}

- (void)keyboardDidShow {
    if (self.textView) {
        self.keyboardView = self.textView.inputAccessoryView.superview;
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    if (self.textView) {
        self.keyboardView = nil;
    }
    [self handleKeyboardNotification:notification animate:NO completion:nil];
}

- (void)handleKeyboardNotification:(NSNotification *)notification animate:(BOOL)animate completion:(void (^)(BOOL finished))completion {
    NSDictionary *userInfo = [notification userInfo];
    
    CGRect keyboardEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (CGRectIsNull(keyboardEndFrame)) {
        return;
    }
    
    CGRect keyboardEndFrameConverted = [self.contextView convertRect:keyboardEndFrame fromView:nil];
    
    if (animate) {
        UIViewAnimationCurve animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
        NSInteger animationCurveOption = (animationCurve << 16);
        
        double animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        [UIView animateWithDuration:animationDuration delay:0.0 options:animationCurveOption animations:^{
            if (self.delegate) {
                [self.delegate keyboardDidChangeFrame:keyboardEndFrameConverted];
            }
        } completion:^(BOOL finished) {
            if (completion) {
                completion(finished);
            }
        }];
    } else {
        if (self.delegate) {
            [self.delegate keyboardDidChangeFrame:keyboardEndFrameConverted];
        }
        
        if (completion) {
            completion(YES);
        }
    }
}

#pragma mark Keyboard View

- (void)setKeyboardView:(UIView *)keyboardView {
    if (_keyboardView) {
        @try {
            if (IOS_8_UP) {
                [_keyboardView removeObserver:self forKeyPath:KVO_CENTER context:nil];
            } else {
                [_keyboardView removeObserver:self forKeyPath:KVO_FRAME context:nil];
            }
        }
        @catch (NSException * __unused exception) { }
    }
    
    _keyboardView = keyboardView;
    
    if (keyboardView) {
        if (IOS_8_UP) {
            [_keyboardView addObserver:self
                            forKeyPath:KVO_CENTER
                               options:(NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew)
                               context:nil];
        } else {
            [_keyboardView addObserver:self
                            forKeyPath:KVO_FRAME
                               options:(NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew)
                               context:nil];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.keyboardView && ([keyPath isEqualToString:KVO_FRAME] || [keyPath isEqualToString:KVO_CENTER])) {
        if (IOS_8_UP) {
            CGRect frame = self.keyboardView.frame;
            if (self.delegate) {
                [self.delegate keyboardDidChangeFrame:[self.contextView convertRect:frame fromView:nil]];
            }
        } else {
            CGRect oldKeyboardFrame = [[change objectForKey:NSKeyValueChangeOldKey] CGRectValue];
            CGRect newKeyboardFrame = [[change objectForKey:NSKeyValueChangeNewKey] CGRectValue];
            
            if (CGRectEqualToRect(newKeyboardFrame, oldKeyboardFrame) || CGRectIsNull(newKeyboardFrame)) {
                return;
            }
            
            if (self.delegate) {
                [self.delegate keyboardDidChangeFrame:[self.contextView convertRect:newKeyboardFrame fromView:nil]];
            }
        }
    }
}

@end

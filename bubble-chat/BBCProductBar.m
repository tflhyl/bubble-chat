//
//  BBCProductBar.m
//  bubble-chat
//
//  Created by Theodore Felix Leo on 23/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//

#import "BBCProductBar.h"

#import <PureLayout/PureLayout.h>
#import <AFNetworking/UIKit+AFNetworking.h>

@interface BBCProductBar ()

@property (weak, nonatomic) UIImageView *imageView;
@property (weak, nonatomic) UILabel *nameLabel;
@property (weak, nonatomic) UILabel *priceLabel;

@end

@implementation BBCProductBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    UIImageView *imageView = [[UIImageView alloc] initForAutoLayout];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = YES;
    [self addSubview:imageView];
    [imageView autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [imageView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [imageView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [imageView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionHeight ofView:imageView];
    self.imageView = imageView;
    
    UILabel *nameLabel = [[UILabel alloc] initForAutoLayout];
    nameLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    nameLabel.textColor = [UIColor darkGrayColor];
    [self addSubview:nameLabel];
    [nameLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
    [nameLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:imageView withOffset:10];
    [nameLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:10];
    self.nameLabel = nameLabel;
    
    UILabel *priceLabel = [[UILabel alloc] initForAutoLayout];
    priceLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    priceLabel.textColor = [UIColor redColor];
    [self addSubview:priceLabel];
    [priceLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:nameLabel withOffset:4];
    [priceLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:nameLabel];
    [priceLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:nameLabel];
    self.priceLabel = priceLabel;
    
    UIView *border = [[UIView alloc] initForAutoLayout];
    border.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
    [self addSubview:border];
    [border autoSetDimension:ALDimensionHeight toSize:0.5];
    [border autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [border autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [border autoPinEdgeToSuperviewEdge:ALEdgeBottom];
}

- (void)setImageURL:(NSURL *)imageURL name:(NSString *)name price:(NSString *)price {
    [self.imageView setImageWithURL:imageURL];
    self.nameLabel.text = name;
    self.priceLabel.text = price;
}

@end

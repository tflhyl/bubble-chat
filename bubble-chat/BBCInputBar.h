//
//  BBCInputBar.h
//  bubble-chat
//
//  Created by Theodore Felix Leo on 23/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BBCInputBar;

@protocol BBCInputBarDelegate <NSObject>

- (void)inputBar:(BBCInputBar *)inputBar didSendMessage:(NSString *)message;

@end

@interface BBCInputBar : UIView

@property (weak, nonatomic, readonly) UITextView *textView;
@property (weak, nonatomic, readonly) UIButton *sendButton;

@property (weak, nonatomic) id<BBCInputBarDelegate> delegate;

@end

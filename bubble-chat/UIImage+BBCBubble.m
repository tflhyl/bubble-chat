//
//  UIImage+BBCBubble.m
//  bubble-chat
//
//  Created by Theodore Felix Leo on 24/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//

#import "UIImage+BBCBubble.h"

@implementation UIImage (BBCBubble)

+ (UIImage *)incomingBubbleImage {
    UIImage *bubble = [UIImage imageNamed:@"bubble-incoming"];
    CGPoint center = CGPointMake(bubble.size.width / 2.0f, bubble.size.height / 2.0f);
    UIEdgeInsets capInsets = UIEdgeInsetsMake(center.y, center.x, center.y, center.x);
    return [bubble resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch];
}

+ (UIImage *)outgoingBubbleImage {
    UIImage *bubble = [UIImage imageNamed:@"bubble-outgoing"];
    CGPoint center = CGPointMake(bubble.size.width / 2.0f, bubble.size.height / 2.0f);
    UIEdgeInsets capInsets = UIEdgeInsetsMake(center.y, center.x, center.y, center.x);
    return [bubble resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch];
}

@end

//
//  AppDelegate.h
//  bubble-chat
//
//  Created by Theodore Felix Leo on 23/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

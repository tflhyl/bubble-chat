//
//  CRSKeyboardController.h
//  Snapsell
//
//  Created by Theodore Felix Leo on 6/6/14.
//
//

#import <UIKit/UIKit.h>

@protocol CRSKeyboardControllerDelegate <NSObject>

- (void)keyboardDidChangeFrame:(CGRect)keyboardFrame;

@end

@interface CRSKeyboardController : NSObject

- (instancetype)initWithTextView:(UITextView *)textView contextView:(UIView *)contextView delegate:(id<CRSKeyboardControllerDelegate>)delegate;
- (instancetype)initWithContextView:(UIView *)contextView delegate:(id<CRSKeyboardControllerDelegate>)delegate;

- (void)beginListeningKeyboard;
- (void)endListeningKeyboard;

@end

//
//  BBCInputBar.m
//  bubble-chat
//
//  Created by Theodore Felix Leo on 23/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//

#import "BBCInputBar.h"

#import <PureLayout/PureLayout.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface BBCInputBar () <UITextViewDelegate>

@property (weak, nonatomic) UITextView *textView;
@property (weak, nonatomic) UIButton *sendButton;

@end

@implementation BBCInputBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.backgroundColor = UIColorFromRGB(0xE7E7E7);
    
    UITextView *textView = [[UITextView alloc] initForAutoLayout];
    textView.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    textView.textColor = [UIColor blackColor];
    textView.editable = YES;
    textView.selectable = YES;
    textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    textView.showsHorizontalScrollIndicator = NO;
    textView.showsVerticalScrollIndicator = YES;
    textView.delegate = self;
    [self addSubview:textView];
    [textView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:8];
    [textView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10];
    [textView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:8];
    self.textView = textView;
    
    UIButton *sendButton = [[UIButton alloc] initForAutoLayout];
    sendButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    [sendButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [sendButton setTitle:@"Send" forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(tapSendButton:) forControlEvents:UIControlEventTouchUpInside];
    sendButton.enabled = NO;
    [self addSubview:sendButton];
    [sendButton autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 8) excludingEdge:ALEdgeLeft];
    [sendButton autoSetDimension:ALDimensionWidth toSize:50];
    [sendButton autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:textView withOffset:8];
    self.sendButton = sendButton;
    
    UIView *border = [[UIView alloc] initForAutoLayout];
    border.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
    [self addSubview:border];
    [border autoSetDimension:ALDimensionHeight toSize:0.5];
    [border autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [border autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [border autoPinEdgeToSuperviewEdge:ALEdgeRight];
}

- (void)tapSendButton:(UIButton *)sendButton {
    NSString *message = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([self.delegate respondsToSelector:@selector(inputBar:didSendMessage:)]) {
        [self.delegate inputBar:self didSendMessage:message];
    }
    self.textView.text = nil;
    self.sendButton.enabled = NO;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    self.sendButton.enabled = textView.text.length > 0;
}

@end

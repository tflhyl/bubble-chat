//
//  NSDateFormatter+CRSExtension.m
//  bubble-chat
//
//  Created by Theodore Felix Leo on 24/5/15.
//  Copyright (c) 2015 Carousell. All rights reserved.
//

#import "NSDateFormatter+CRSExtension.h"

@interface NSDate (CRSExtension)

- (BOOL)crs_isToday;
- (BOOL)crs_isThisYear;

@end

@implementation NSDate (CRSExtension)

- (BOOL)crs_isToday {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit calendarUnit = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    
    NSDate *today = [calendar dateFromComponents:[calendar components:calendarUnit fromDate:[NSDate date]]];
    NSDate *comparedDay = [calendar dateFromComponents:[calendar components:calendarUnit fromDate:self]];
    
    return [today isEqualToDate:comparedDay];
}

- (BOOL)crs_isThisYear {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit calendarUnit = NSCalendarUnitEra | NSCalendarUnitYear;
    
    NSDate *thisYear = [calendar dateFromComponents:[calendar components:calendarUnit fromDate:[NSDate date]]];
    NSDate *comparedYear = [calendar dateFromComponents:[calendar components:calendarUnit fromDate:self]];
    
    return [thisYear isEqualToDate:comparedYear];
}

@end

@implementation NSDateFormatter (CRSExtension)

+ (NSDateFormatter *)crs_sharedInstance {
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateFormatter = threadDictionary[@"dateFormatter"];
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        threadDictionary[@"dateFormatter"] = dateFormatter;
    }
    return dateFormatter;
}

- (NSDate *)crs_dateFromAPIString:(NSString *)string {
    self.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    self.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    self.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    return [self dateFromString:string];
}

- (NSString *)crs_stringForTimestampFromDate:(NSDate *)date {
    if ([date crs_isToday]) {
        self.dateFormat = [NSString stringWithFormat:@"%@ HH:mm", NSLocalizedString(@"DateFormat-Today", nil)];
    } else if ([date crs_isThisYear]) {
        self.dateFormat = @"dd MMM HH:mm";
    } else {
        self.dateFormat = @"dd MMM yyyy HH:mm";
    }
    
    return [[self stringFromDate:date] lowercaseString];
}

@end
